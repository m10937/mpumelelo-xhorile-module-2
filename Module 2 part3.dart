class mtnApp {
  String? name;
  String? category;
  String? developer;
  int? year;

  void showAppInfo() {
    name = name?.toUpperCase();
    print(name);
    print(category);
    print(developer);
    print(year);
  }
}

void main() {
  mtnApp mtnapp1 = mtnApp();
  mtnapp1.name = "Shyft";
  mtnapp1.category = "Best Financial Solution";
  mtnapp1.developer = "Standard Bank";
  mtnapp1.year = 2017;
  mtnapp1.showAppInfo();
}
